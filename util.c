#include "util.h"

void
err(char *message)
{
    fprintf(stderr, "%s (%s)\n", message, strerror(errno));
}
