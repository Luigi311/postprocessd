#ifndef POSTPROCESSD__POSTPROCESS_H
#define POSTPROCESSD__POSTPROCESS_H

#include <stdint.h>
#include "util.h"

#define TIFFTAG_FORWARDMATRIX1 50964

#define JPEG_APP1 JPEG_APP0+1

struct Imagedata {
        uint32_t width;
        uint32_t height;
        uint8_t bitspersample;
        char *make;
        char *model;
        char *software;
        int orientation;
        char *datetime;
        uint16_t exposure_program;
        float exposure_time;
        uint16_t isospeed;
        int flash;
        float fnumber;
        float focal_length;
        uint16_t focal_length_35mm;
};

void
postprocess_internal(char *burst_dir, char *target_dir, int keep);

void
postprocess_single(char *in_path, char *out_path, int quality, int verbose);

void
postprocess_setup();

#endif //POSTPROCESSD__POSTPROCESS_H
