# postprocessd

This is a project that creates a native postprocessing pipeline for Megapixels that aims to improve the image quality.

## Usage

Process a single image without stacking:

```shell-session
$ postprocess-single input.dng output.jpg
```

Process a burst with stacking

```shell-session
This is still WIP, api compatible with postprocess.sh
$ postprocessd /tmp/megapixels-something /home/user/Pictures/my-photo 1
```
